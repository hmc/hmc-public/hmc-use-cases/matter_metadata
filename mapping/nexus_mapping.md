# Mapping to NeXus fields

| No. | Name | defined in NeXus / internal NeXus path                                       |
| --- | ---  |------------------------------------------------------------------------------|
| 1. | kernelInformationProfile | &cross; (:unap)                                                              |
| 2. | digitalObjectType | &cross; (:unap)                                                              | 
| 3. | digitalObjectLocation | &cross; (:unap)                                                              | 
| 4. | digitalObjectLocationAccessProtocol | &cross; (:unap)                                                              | 
| 5. | dateCreated | &check; /root@file_time                                                      |
| 6. | dateModified | &cross; /root@file_update_time                                               | 
| 7. | underEmbargoUntil | &cross; (:unap)                                                              | 
| 8. | digitalObjectPolicy | &cross;                                                                      |
| 9. | version | &check; /entry/revision                                                      | 
| 10. | license | &cross;                                                                      | 
| 11. | checksum | &cross; (:unap)                                                              | 
| 12. | signature | &cross; (:unap)                                                              | 
| 13. | topic | &cross;                                                                      | 
| 14. | locationPreview / locationSample | &check;                                                                      | /entry/thumbnail                                                     | 
| 15. | contact | &check; /entry/USER                                                          | 
| 16. | hasMetadata | &cross;                                                                      | 
| 17. | isMetadataFor | &cross;                                                                      | 
| 18. | wasGeneratedBy | &check; instrument: /entry/instrument/name<br/>software: /entry/program_name | 
| 19. | wasDerivedFrom | &cross;                                                                      | 
| 20. | specializationOf | &cross;                                                                      | 
| 21. | wasRevisionOf | &cross;                                                                      | 
| 22. | hadPrimarySource | &cross;                                                                      | 
| 23. | wasQuotedFrom | &cross;                                                                      | 
| 24. | alternateOf | &cross;                                                                      | 
| 25. | provenanceGraph | &cross;                                                                      | 
| 26. | digitalObjectCategory | &cross;                                                                      | 
| 27. | dateStart | &check; /entry/start_time                                                    | 
| 28. | dateEnd | &check; /entry/end_time                                                      | 
| 29. | beamProbe | &check; /entry/instrument/SOURCE/probe                                       | 
| 29.a | beamEnergy | &check; /entry/instrument/SOURCE/energy                                      |
| 29.b | beamEnergyUnit | &check; /entry/instrument/SOURCE/energy@units                                |
| 29.c | beamSourceType | &check; /entry/instrument/SOURCE/type                                        |
| 29.c | beamSourceIdentifier | &cross; |
| 30. | detectorAngleLower | &cross;                                                                      | 
| 30.a | detectorAngleUpper | &cross;                                                                      | 
| 30.b | detectorEnergyLower | &cross;                                                                      | 
| 30.c | detectorEnergyUpper | &cross;                                                                      | 
| 30.d | detectorEnergyUnits | &cross;                                                                      | 
| 31 | sample | &check; /entry/sample/name                                                   | 
| 31.a | sampleFormula | &check; /entry/sample/chemical_formula                                       | 
| 31.b | sampleClass | &cross;                                                                      | 
| 31.c | sampleTemperature | &check; /entry/sample/temperature                                            | 
| 31.d | samplePressure | &check; /entry/sample/pressure                                               | 
| 31.e | sampleMagneticField | &check; /entry/sample/magnetic_field                                         | 
| 32  | targetPrimary | &cross;                                                                      | 
| 32.a | targetSecondary | &cross;                                                                      | 

_Note: ":unap" (dataCite: "not applicable, makes no sense") means that this kind of information is not available at the file level. "/entry" belongs to the default entry indicated by /root@default if there is more than one entry._
