# Example Metadata Set #1

| No.  | Name                  | Value                                        | Comment                             |
|------|-----------------------|----------------------------------------------|-------------------------------------|
| 5.   | dateCreated           | 2022-12-22T12:58:01+01:00                    |                                     |
| 10.  | license               | CC BY-NC 4.0                                 | Defined by PID                      |
| 13.  | topic                 | tomography                                   |                                     |
| 13.a | topicScheme           | PaNET                                        |                                     |
| 13.b | topicIdentifier       | http://purl.org/pan-science/PaNET/PaNET01129 |                                     |
| 18.  | wasGeneratedBy        | CONRAD II                                    | Defined by Instrument PID           |
| 26.  | digitalObjectCategory | measurement data                             | Defined by [HDO](https://codebase.helmholtz.cloud/hmc/hmc-public/hob/hdo) (Ontology/IRI)             |
| 27.  | dateStart             | 2022-01-01T13:28:22+01:00                    |                                     |
| 28.  | dateEnd               | 2022-01-01T13:28:22+01:00                    |                                     |
| 29.  | beamProbe             | neutron                                      |                                     |
| 29.a | beamEnergy            | 9.1                                          |                                    |
| 29.b | beamEnergyUnit        | meV                                          |                                    |
| 29.c | beamSourceType        | Reactor Neutron Source                       |                                     |
| 29.d | beamSourceIdentifier  | Research Reactor BER II                      | Defined by ROR (doesn't exist yet)  |
| 30.  | detectorAngleLower    | 0                                            |                                     |
| 30.a | detectorAngleUpper    | 360                                          |                                     |
| 31   | sample                | YBCO_Salt                                    |                                     |
| 31.b | sampleClass           | crystalline material                         |                                     |
| 31.c | sampleTemperature     | 300                                          | default units: K                    |

<details>
  <summary>Click to show description</summary>
  Metadata describe a measurement of YBCO salt sample performed at the cold neutron tomography instrument CONRAD II at the BER II reactor (HZB Wannsee). Data is collected by exposing the sample at approx. room temperature to a neutron beam of wavelength of 3 Angstrom (9.1 meV) while rotating the sample from 0 to 360 degrees step-wise.
</details>

_Note: Rather bibliographic and technical metadata, such as contact or checksum, are omitted; PIDs are resolved to support human-readability (noted as a comment)._
