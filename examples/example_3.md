# Example Metadata Set #3

| No.  | Name                  | Value                                       | Comment                           |
|------|-----------------------|---------------------------------------------|-----------------------------------|
| 5.   | dateCreated           | 2023-08-25T10:02:37.258376+01:00            |                                   |
| 13.  | topic                 | ComputationalPhysics                        |                                   | 
| 13.a | topicScheme           | ModSci                                      |                                   |
| 13.b | topicIdentifier       | https://w3id.org/skgo/modsci#ParticlePhysics |                                   |
| 26.  | digitalObjectCategory | simulation data                             | Defined by [HDO](https://codebase.helmholtz.cloud/hmc/hmc-public/hob/hdo) (Ontology/IRI) | 
| 27.  | dateStart             | 2008-04-15T09:45:12+01:00                   |                                   | 
| 28.  | dateEnd               | 2008-04-16T13:04:26+01:00                   |                                   | 
| 31   | sample                | confined Lennard-Jones fluid                |                                   |
| 31.b | sampleClass           | nanomaterial, confined fluid                |                                   |

<details>
  <summary>Click to show description</summary>
  Metadata describe simulation output data (of self-written Fortran program) whose system comprises a simple Lennard-Jones fluid confined in slit-pore geometry of varying widths.

  _Note: Would be helpful to have detailed simulation procedures for the topic field, e.g. Monte Carlo Simulation or Molecular Dynamics Simulation._

</details>

_Note: Rather bibliographic and technical metadata, such as contact or checksum, are omitted; PIDs are resolved to support human-readability (noted as a comment)._
