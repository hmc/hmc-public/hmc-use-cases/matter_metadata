# Example Metadata Set #2

| No.  | Name                            | Value                                        | Comment                                                                                  |
|------|---------------------------------|----------------------------------------------|------------------------------------------------------------------------------------------|
| 5.   | dateCreated                     | 2023-08-25T10:02:37.258376+01:00             |                                                                                          |
| 10.  | license                         | CC BY-NC 4.0                                 | Defined by PID                                                                           | 
| 13.  | topic                           | Particle Physics                             |                                                                                          | 
| 13.a | topicScheme                     | ModSci                                       |                                                                                          |
| 13.b | topicIdentifier                 | https://w3id.org/skgo/modsci#ParticlePhysics |                                                                                          |
| 18.  | wasGeneratedBy                  | A4                                           | Defined by Instrument PID                                                                | 
| 26.  | digitalObjectCategory           | measurement data                             | Defined by [HDO](https://codebase.helmholtz.cloud/hmc/hmc-public/hob/hdo) (Ontology/IRI) | 
| 27.  | dateStart                       | 2010-10-15T15:16:33+02:00                    |                                                                                          | 
| 28.  | dateEnd                         | 2010-11-03T11:43:36+01:00                    |                                                                                          | 
| 29.  | beamProbe                       | electron                                     |                                                                                          | 
| 29.a | beamEnergy                      | 0.032, 0.057, 0.082, 0.218, 0.613            |                                                                                          |
| 29.b | beamEnergyUnit                  | (GeV/c)^2                                    |                                                                                          |
| 29.c | beamSourceType                  | Microtron                                    |                                                                                          |
| 29.d | beamSourceIdentifier            | MAMI                                         | Defined by ROR (doesn't exist yet)                                                       |
| 32   | targetPrimary                   | H2/D2                                        |                                                                                          |

<details>
  <summary>Click to show description</summary>
  Metadata describe a measurement of the A4 instrument at the Mainz Microtron (MAMI) were the beam transverse single spin asymmetry in electron-proton elastic scattering at beam energies from 315.1 to 1508.4 MeV and at a scattering angle of 30 < I < 40 were measured. The covered Q2 values are 0.032, 0.057, 0.082, 0.218, 0.613 (GeV/c)^2.
</details>

_Note: Rather bibliographic and technical metadata, such as contact or checksum, are omitted; PIDs are resolved to support human-readability (noted as a comment)._
