# Example Metadata Set #7

| No.  | Name                  | Value                                     | Comment                                       |
|------|-----------------------|-------------------------------------------|-----------------------------------------------|
| 5.   | dateCreated           | 2023-11-24                                |                                               | 
| 13.  | topic                 | Spectroscopy                              |                                               | 
| 13.a | topicScheme           | ModSci                                    |                                               |
| 13.b | topicIdentifier       | https://w3id.org/skgo/modsci#Spectroscopy |                                               |
| 18.  | wasGeneratedBy        | POLARIS                                   | Defined by Instrument PID (doesn't exist yet) | 
| 26.  | digitalObjectCategory | measurement data                          | Defined by [HDO](https://codebase.helmholtz.cloud/hmc/hmc-public/hob/hdo) (Ontology/IRI)                       | 
| 29.  | beamProbe             | infrared                                  |                                               | 
| 29.a | beamEnergy            | 1.2                                       |                                               |
| 29.b | beamEnergyUnit        | eV                                        |                                               |
| 29.c | beamSourceType        | Optical Laser                             |                                               |
| 31   | sample                | H2O                                       |                                               |
| 31.b | sampleClass           | fluid, droplet                            |                                               |         
<details>
  <summary>Click to show description</summary>
  Metadata describes a measurement of the POLARIS laboratory using a ultra-high intensity laser to perform a proton acceleration experiment on a mass-limited target, in this case H2O dropplets.
</details>

_Note: Rather bibliographic and technical metadata, such as contact or checksum, are omitted; PIDs are resolved to support human-readability (noted as a comment)._
