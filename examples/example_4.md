# Example Metadata Set #4

| No.  | Name                  | Value                                        | Comment                            |
|------|-----------------------|----------------------------------------------|------------------------------------|
| 5.   | dateCreated           | 2021-09-03T12:32:15+02:00                    |                                    |
| 13.  | topic                 | x-ray absorption spectroscopy                |                                    | 
| 13.a | topicScheme           | PaNET                                        |                                    |
| 13.b | topicIdentifier       | http://purl.org/pan-science/PaNET/PaNET01196 |                                    |
| 18.  | wasGeneratedBy        | Iontrap                                      | Defined by Instrument PID          | 
| 26.  | digitalObjectCategory | measurement data                             | Defined by [HDO](https://codebase.helmholtz.cloud/hmc/hmc-public/hob/hdo) (Ontology/IRI)            | 
| 27.  | dateStart             | 2017-10-16T20:06:09+02:00                    |                                    | 
| 28.  | dateEnd               | 2017-10-16T21:09:32+02:00                    |                                    | 
| 29.  | beamProbe             | x-ray                                        |                                    | 
| 29.a | beamEnergy            | 390.0                                        |                                    |
| 29.b | beamEnergyUnit        | MeV                                          |                                    |
| 29.c | beamSourceType        | Synchrotron X-ray Source                     |                                    |
| 29.d | beamSourceIdentifier  | BESSY II                                     | Defined by ROR (doesn't exist yet) |
| 31   | sample                | Molecular nitrogen cation (gas phase)        |                                    |
| 31.a | sampleFormula         | N_2^+                                        |                                    | 
| 31.b | sampleClass           | gaseous material                             |                                    |
| 31.c | sampleTemperature     | 14.4                                         | default units: K                   |

<details>
  <summary>Click to show description</summary>
  Metadata describe a measurement of the Iontrap beamline at BESSY II Synchrotron. Here, nitrogen cations are created in a gas phase and exposed to a x-ray beam of 390 MeV while absorption spectrum is measured.
</details>

_Note: Rather bibliographic and technical metadata, such as contact or checksum, are omitted; PIDs are resolved to support human-readability (noted as a comment)._
