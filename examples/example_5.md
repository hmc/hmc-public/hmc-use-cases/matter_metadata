# Example Metadata Set #5

| No. | Name                  | Value                        | Comment                 |
|-----|-----------------------|---------------------------|-------------------------|
| 5.  | dateCreated           | 2023-10-12T00:00:00+01:00 |                         |
| 9.  | version               | v6.8.0                    |                         |
| 10. | license               | GPLv3                     | Defined by PID          | 
| 26. | digitalObjectCategory | software                  | Defined by [HDO](https://codebase.helmholtz.cloud/hmc/hmc-public/hob/hdo) (Ontology/IRI) |

<details>
  <summary>Click to show description</summary>
  Metadata describe the MANTID software for analysing neutron and muon experimental data (https://www.mantidproject.org/index.html).

  _Note: Would be helpful to have classes of software which could be detailed in topic, topicScheme, and topicIdentifier, e.g. to distinguish between software for data evaluation and simulation programs._

</details>

_Note: Rather bibliographic and technical metadata, such as contact or checksum, are omitted; PIDs are resolved to support human-readability (noted as a comment)._

