# Example Metadata Set #6

| No.  | Name                            | Value                     | Comment                            |
|------|---------------------------------|---------------------------|------------------------------------|
| 5.   | dateCreated                     | 2023-08-08                |                                    |
| 10.  | license                         | CC BY 4.0                 | Defined by PID                     | 
| 13.  | topic                           | Nuclear Reaction Analysis |                                    | 
| 13.a | topicScheme                     | IBA                       |                                    |
| 18.  | wasGeneratedBy                  | COMPASS                   | Defined by Instrument PID          | 
| 26.  | digitalObjectCategory           | measurement data          | Defined by [HDO](https://codebase.helmholtz.cloud/hmc/hmc-public/hob/hdo) (Ontology/IRI)            | 
| 27.  | dateStart                       | 2015-10-10                |                                    | 
| 28.  | dateEnd                         | 2016-03-20                |                                    | 
| 29.  | beamProbe                       | ion                       |                                    | 
| 29.a | beamEnergy                      | 4.4                       |                                    |
| 29.b | beamEnergyUnit                  | MeV/u                     |                                    |
| 29.c | beamSourceType                  | Ion Source                |                                    |
| 29.d | beamSourceIdentifier            | UNILAC                    | Defined by ROR (doesn't exist yet) |
| 32   | targetPrimary                   | Tantalum-181              |                                    |

<details>
  <summary>Click to show description</summary>
  Metadata describes a measurement of the COMPASS instrument (COMPAct decay Spectroscopy Set-up) at GSI investigating the 48Ca+181Ta reaction: Cross section studies and investigation of neutron-deficient 86 ≤ Z ≤ 93 isotopes.
</details>

_Note: Rather bibliographic and technical metadata, such as contact or checksum, are omitted; PIDs are resolved to support human-readability (noted as a comment)._
