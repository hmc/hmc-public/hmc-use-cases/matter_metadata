# Helmholtz Hub Matter Kernel Information Profile 
The idea behind a custom Helmholtz Kernel Information Profile (KIP) is to define a Helmholtz-wide minimal KIP to be able to exchange FAIR DOs in a standardized way. Based on the Kernel Information Profile recommendations of the RDA, the Helmholtz KIP was extended by fields which are required to create a minimal FAIR DO in the shared view of the HMC Hubs. We expect that many discipline-specific KIPs will emerge, since every discipline has its own requirements about data and metadata. By basing all discipline-specific KIPs to a Helmholtz KIP we can guarantee minimal interdisciplinary interoperability. [...]

One important aspect of FAIR DOs is to have PIDs with rich metadata stored in the record. The amount and kind of stored information will depend on many aspects like disciplines, the kind of data, legacy aspects and more. For interoperability, it is important to have this information available in a structured and machine-actionable way. The Helmholtz KIP defines recommendations about which metadata should be available. [[1]](https://doi.org/10.3289/HMC_publ_03)

_[Note: Some simplifications were made in the following table, e.g. comments were removed in which cases the cardinality changes from optional to mandatory.]_

| No. | Name | Cardinality | Format | Description |
| --- | ---  | ---         | ---    | ---         |
| 1. | kernelInformationProfile | 1 (mandatory) | PID | A PID pointing to the KIP which provides the structure of this PID record. |
| 2. | digitalObjectType | 1 (mandatory) | PID | A PID pointing to a data type in a Data Type Registry describing the object referenced by _digitalObjectLocation_. |
| 3. | digitalObjectLocation | 1+ (mandatory, repeatable) | URL | A web-resolvable pointer to the actual object described by this PID record. |
| 4. | digitalObjectLocationAccessProtocol | 0/1 (optional) | String | Additional information which can be used for accessing digitalObjectLocation. |
| 5. | dateCreated | 1 (mandatory) | ISO Date/Time | The date in ISO 8601 format, when the object referenced by digitalObjectLocation was initially created. |
| 6. | dateModified | 0/1 (optional) | ISO Date/Time | The date in ISO 8601 format, when the object referenced by digitalObjectLocation was modified. |
| 7. | underEmbargoUntil | 0/1 (optional) | ISO Date/Time | The date in ISO 8601 format, when the object referenced by digitalObjectLocation is planned to be publicly accessible or was made publicly accessible. |
| 8. | digitalObjectPolicy | 0/1 (optional) | PID | A web-resolvable pointer to a policy object describing e.g., object access and modification policies. |
| 9. | version | 0/1 (optional) | Integer or String | Version of the object referenced by digitalObjectLocation, either as single number or following semantic versioning conventions. |
| 10. | license | 0 (optional) | URL | URL referring to the license of the object referenced by digitalObjectLocation, its modifiers (if applicable), and its version. |
| 11. | checksum | 1 (mandatory) | Formatted String | Checksum of the object referenced by digitalObjectLocation in any supported checksum format. |
| 12. | signature | 0+ (optional) | Formatted String? | A cryptographic signature of this record in a specified format, including especially the checksum for advanced integrity checks and assumptions about reproducibility. |
| 13. | topic | 0+ (optional) | Controlled Vocabulary | One or more topics the object referenced by digitalObjectLocation is related to. |
| 14. | locationPreview / locationSample | 0+ (optional) | URL | A web-resolvable pointer to a preview, e.g., a low-resolution image, of the object referenced by digitalObjectLocation. |
| 15. | contact | 0+ (optional) | URL | A web-resolvable pointer to an institution or a person responsible for the object referenced by digitalObjectLocation. |
| 16. | hasMetadata | 0+ (optional) | PID | One or more PID(s) referring to related FAIR DOs providing metadata for the object referenced by digitalObjectLocation. |
| 17. | isMetadataFor | 0+ (optional) | PID | A PID pointing to another FAIR DO describing the object referenced by digitalObjectLocation. |
| 18. | wasGeneratedBy | 0/1 (optional) | PID | A PID pointing to an activity which generated the object referenced by digitalObjectLocation. |
| 19. | wasDerivedFrom | 0+ (optional) | PID | A PID pointing to another FAIR DO from which the object referenced by digitalObjectLocation was derived from. |
| 20. | specializationOf | 0+ (optional) | PID | A PID pointing to another FAIR DO which is a specialization of the object referenced by digitalObjectLocation. |
| 21. | wasRevisionOf | 0+ (optional) | PID | A PID pointing to another FAIR DO which was revised to create the object referenced by _digitalObjectLocation_.? |
| 22. | hadPrimarySource | 0+ (optional) | PID | A PID pointing to another FAIR DO which was a primary source for the creation of the object referenced by _digitalObjectLocation_.? |
| 23. | wasQuotedFrom | 0+ (optional) | PID | A PID pointing to another FAIR DO from which the object referenced by digitalObjectLocation was fully or partly quoted. |
| 24. | alternateOf | 0+ (optional) | PID | A PID pointing to another FAIR DO which is an alternate of the object referenced by digitalObjectLocation. |
| 25. | provenanceGraph | 0/1 (optional) | PID | A PID pointing to another FAIR DO which refers to the provenance graph of the object referenced by digitalObjectLocation |

_Note: An issue in the corresponding gitlab project of CCT4 is created to address some concerns; see [#34](https://codebase.helmholtz.cloud/hmc/hmc/cct4-from-development-to-deployment/guidance-hmc-generic-base-kernel-information-profile/-/issues/34)._

# Helmholtz Hub Matter Information Profile
The Helmholtz Kernel Information Profile is supposed to be extended by Hub specific fields that characterize a (meta)data. Here, we focus on a minimal HKIP extension for Hub Matter that allows a machine to classify the kind of data roughly. As a result we could increase

- **Findability** by allowing more specific searches for data that, otherwise, would be buried in an indistinguishable bulk of search results.
- **Interoperability** by providing additional information, e.g. about the data's file format that allows a machine to choose an application to automatically read the file.

## Recommended Use of HKIP Fields & Extension
The Helmholtz Kernel Information Profile contains general bibliographic (fields 1-15) as well as optional provenance information (fields 16-24) that can characterize a data set if available and properly used. The usage of existing fields could be recommended as follows:

### topic

| No.  | Name            | Cardinality    | Format                | Description                                                                                                                                                                                                                                                                                                  |
|------|-----------------|----------------|-----------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 13.  | topic           | 0+ (optional)  | Controlled Vocabulary | One or more terms to classify a scientific topic of the object referenced by digitalObjectLocation; chosen from a proposed topicScheme.                                                                                                                                                                                                            |
| 13.a | topicScheme     | 0/1 (optional) | Controlled Vocabulary | The Scheme the topic is derived from: [ModSci](https://saidfathalla.github.io/Science-knowledge-graph-ontologies/doc/ModSci_doc/ontology.rdf), [PaNET](https://github.com/ExPaNDS-eu/ExPaNDS-experimental-techniques-ontology/blob/master/source/PaNET.owl), [IBA](https://doi.org/10.1088/1741-4326/ab5817) |
| 13.b | topicIdentifier | 0/1 (optional) | PID                   | A PID that defines the topic, e.g. pointing to the term of ModSci or PaNET.                                                                                                                                                                                                                                  |
Comments:
- [ModSci](https://saidfathalla.github.io/Science-knowledge-graph-ontologies/doc/ModSci_doc/ontology.rdf) provides general classes to roughly assign a scientific field, e.g.
  - [Particle Physics](https://w3id.org/skgo/modsci#ParticlePhysics)
  - [Spectroscopy](https://w3id.org/skgo/modsci#Spectroscopy)
  - I opened issue [#7](https://github.com/saidfathalla/Science-knowledge-graph-ontologies/issues/7) to extend the Ontology
- [PaNET](https://github.com/ExPaNDS-eu/ExPaNDS-experimental-techniques-ontology/blob/master/source/PaNET.owl) can be used to classify x-Ray and neutron techniques, e.g.
  - [x-ray diffraction](http://purl.org/pan-science/PaNET/PaNET01216)
  - [inelastic neutron scattering](http://purl.org/pan-science/PaNET/PaNET01245)
- [IBA](https://doi.org/10.1088/1741-4326/ab5817) provides a controlled vocabulary to classify ion beam analysis techniques, e.g.
  - Electron Probe Micro-Analysis
  - Nuclear Reaction Analysis

_Note: It would be advantageous to use only a single ontology to increase findability (using the same terms) but I haven't seen an appropriate one yet._

_Note: Would be helpful to have classes of software which can be detailed in topic, topicScheme, and topicIdentifier; e.g. to distinguish software for analysis and simulation._

_Note: Would be helpful to have detailed simulation procedures for the topic field, e.g. Monte Carlo Simulation or Molecular Dynamics Simulation._

### wasGeneratedBy

| No. | Name | Cardinality | Format | Description                                                                                                                                                        |
| --- | ---  | ---         | ---    |--------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 18. | wasGeneratedBy | 0/1 (optional) | PID | An web-resolvable [Instrument PID](https://doi.org/10.15497/RDA00070) or a PID pointing to a software that created the object referenced by digitalObjectLocation. |

## Matter-Specific Fields
A separate Helmholtz Matter Kernel Information Profile (HM-KIP) will be created for the metadata schema which includes the HKIP as well as the following extension. The HM-KIP will define expected content and description of the fields (see e.g. [21.T11148/0c5636e4d82b88f86132](http://dtr-test.pidconsortium.eu/#objects/21.T11148/0c5636e4d82b88f86132)).

### digitalObjectCategory

| No. | Name                  | Cardinality | Format | Description                                                                                                                                                                                                                                                                                                                                                                                                                 |
|-----|-----------------------| ---         |-----|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 26. | digitalObjectCategory | 0/1 (optional) | PID | PURL providing an arbitrary classification of the object referenced by digitalObjectLocation in terms of the [Helmholtz Digitisation Ontology](https://codebase.helmholtz.cloud/hmc/hmc-public/hob/hdo): [measurement data](https://purls.helmholtz-metadaten.de/hob/HDO_00001071), [computational data](https://purls.helmholtz-metadaten.de/hob/HDO_00001069), or [software](http://purl.obolibrary.org/obo/IAO_0000010). |

_Note: "paper publication" (text, e.g. a paper) or "combined datum" (containing measurement and simulation data) could be helpful._

### dateStart

| No. | Name      | Cardinality | Format | Description |
|-----|-----------| ---         | ---    | ---         |
| 27. | dateStart | 0/1 (optional) | ISO Date/Time | The date in ISO 8601 format when a process (measurement or computation) started that creates the data. |

_Note: Could differ significantly from _dateCreated_ depending on the upload interval (e.g. converting 'old' data)._

### dateEnd

| No. | Name    | Cardinality | Format | Description                                                                                           |
|-----|---------| ---         | ---    |-------------------------------------------------------------------------------------------------------|
| 28. | dateEnd | 0/1 (optional) | ISO Date/Time | The date in ISO 8601 format when a process (measurement or computation) ceases that creates the data. |

_Note: Could differ significantly from _dateStart_, e.g. computations lasting for weeks._

### beamProbe
| No. | Name      | Cardinality | Format | Description                                                                                                                                                                                                                                                                                                                                       |
|-----|-----------| ---         | ---    |---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 29. | beamProbe | 0/1 (optional) | controlled vocabulary | The type of probe that was used to measure the object referenced by digitalObjectLocation: neutron, x-ray, muon, electron, ultraviolet, visible light, positron, proton, ion; corresponds to the list defined in probe of [NXsource](https://manual.nexusformat.org/classes/base_classes/NXsource.html#nxsource-probe-field) + "ion", "infrared". |

### beamEnergy
| No.  | Name       | Cardinality   | Format | Description                                                                                            |
|------|------------|---------------| ---    |--------------------------------------------------------------------------------------------------------|
| 29.a | beamEnergy | 0+ (optional) | Float | The (average) incident energy that was used to measure the object referenced by digitalObjectLocation. |

### beamEnergyUnits
| No.  | Name           | Cardinality    | Format                | Description                                                                                    |
|------|----------------|----------------|-----------------------|------------------------------------------------------------------------------------------------|
| 29.b | beamEnergyUnit | 0/1 (optional) | controlled vocabulary | The units in which beamEnergy is provided: eV or MeV/u; mandatory if a beamEnergy is provided. |

### beamSourceType
| No.  | Name           | Cardinality    | Format                | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
|------|----------------|----------------|-----------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 29.c | beamSourceType | 0/1 (optional) | controlled vocabulary | The type which produces the beamProbe: Spallation Neutron Source, Pulsed Reactor Neutron Source, Reactor Neutron Source, Synchrotron X-ray Source, Pulsed Muon Source, Rotating Anode X-ray, Fixed Tube X-ray, UV Laser, Free-Electron Laser, Optical Laser, Ion Source, UV Plasma Source, Metal Jet X-ray, Microtron; list corresponds to the one allowed in type of [NXsource](https://manual.nexusformat.org/classes/base_classes/NXsource.html#nxsource-type-field) + "Microtron". |

### beamSourceIdentifier
| No.  | Name                 | Cardinality    | Format | Description                                                                                                                                                    |
|------|----------------------|----------------|--------|----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 29.d | beamSourceIdentifier | 0/1 (optional) | PID    | A web-resolvable [Instrument PID](https://doi.org/10.15497/RDA00070) or [ROR](https://ror.org/) which defines the instrument/facility producing the beamProbe. |

### detectorAngleLower
| No. | Name               | Cardinality | Format | Description                                                                                                                   |
|-----|--------------------| ---         | ---    |-------------------------------------------------------------------------------------------------------------------------------|
| 30. | detectorAngleLower | 0/1 (optional) | Float | The lower boundary of the detector angle in degrees when measuring the object referenced by digitalObjectLocation. |

### detectorAngleUpper
| No.  | Name               | Cardinality | Format | Description |
|------|--------------------| ---         | ---    | ---         |
| 30.a | detectorAngleUpper | 0/1 (optional) | Float | The upper boundary of the detector angle in degrees when measuring the object referenced by digitalObjectLocation. |

### detectorEnergyLower
| No.  | Name | Cardinality | Format | Description |
|------| ---  | ---         | ---    | ---         |
| 30.b | detectorEnergyLower | 0/1 (optional) | Float | The lower boundary of the energy range related to the object referenced by digitalObjectLocation. |

### detectorEnergyUpper
| No.  | Name | Cardinality | Format | Description |
|------| ---  | ---         | ---    | ---         |
| 30.c | detectorEnergyUpper | 0/1 (optional) | Float | The upper boundary of the energy range related to the object referenced by digitalObjectLocation. |

### detectorEnergyUnits
| No.  | Name                | Cardinality | Format | Description                                                                                           |
|------|---------------------| ---         |--------|-------------------------------------------------------------------------------------------------------|
| 30.d | detectorEnergyUnits | 0/1 (optional) | Controlled Vocabulary | The units in which beamEnergy is provided: eV or MeV/u; mandatory if detectorEnergyLower is provided. |

### sample
| No. | Name          | Cardinality | Format | Description                         |
|----|---------------| ---         |--------|-------------------------------------|
| 31 | sample | 0/1 (optional) | String | The name of the sample, e.g. water. |

### sampleFormula
| No.  | Name          | Cardinality | Format | Description                                                                                                                        |
|------|---------------| ---         |--------|------------------------------------------------------------------------------------------------------------------------------------|
| 31.a | sampleFormula | 0/1 (optional) | String | The chemical formula of the sample according to [IUPAC](https://iupac.org/wp-content/uploads/2016/07/Red_Book_2005.pdf), e.g. H2O. |

### sampleClass
| No.  | Name        | Cardinality   | Format | Description                                                                                                                                                                                 |
|------|-------------|---------------|------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 31.b | sampleClass | 0+ (optional) | PID  | A PID refering to an (artififcial) class the sample belongs to (could be more than one) which is selected from [EMMO](https://github.com/emmo-repo/EMMO), e.g. ContinuumMaterial or Liquid. |

_Note: The ontology could be streamlined (re-order amorphous/crystalline classes), enriched with [TFSCO](https://matportal.org/ontologies/TFSCO) terms (e.g. thin film), and integrated into the [BFO](https://basic-formal-ontology.org/) hierarchy._

### sampleTemperature
| No.  | Name          | Cardinality   | Format | Description                                            |
|------|---------------|---------------|--------|--------------------------------------------------------|
| 31.c | sampleTemperature | 0+ (optional) | Float  | The (average) temperature of the sample in Kelvin (K). |

### samplePressure
| No.  | Name           | Cardinality   | Format | Description                                  |
|------|----------------|---------------|--------|----------------------------------------------|
| 31.d | samplePressure | 0+ (optional) | Float  | The (average) pressure of the sample in bar. |

### sampleMagneticField
| No.  | Name           | Cardinality   | Format | Description                                              |
|------|----------------|---------------|--------|----------------------------------------------------------|
| 31.e | sampleMagneticField | 0+ (optional) | Float  | The (average) magnetic field of the sample in Tesla (T). |

### targetPrimary
| No. | Name          | Cardinality | Format | Description                                                                                                                                             |
|-----|---------------| ---         |--------|---------------------------------------------------------------------------------------------------------------------------------------------------------|
| 32  | targetPrimary | 0/1 (optional) | Controlled Vocabulary | The name of the primary target according to [IUPAC](https://iupac.org/wp-content/uploads/2016/07/Red_Book_2005.pdf), e.g. Beryllium-9 or Tantalum-181. |

### targetSecondary
| No. | Name            | Cardinality | Format                | Description                                                                                                                                             |
|-----|-----------------| ---         |-----------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------|
| 33  | targetSecondary | 0/1 (optional) | Controlled Vocabulary | The name of the primary target according to [IUPAC](https://iupac.org/wp-content/uploads/2016/07/Red_Book_2005.pdf), e.g. Beryllium-9 or Tantalum-181. |


## Comments

- Standard units (e.g. 'nm') can be defined in the KIP (see e.g. [21.T11148/0c5636e4d82b88f86132](http://dtr-test.pidconsortium.eu/#objects/21.T11148/0c5636e4d82b88f86132)).
- what about file size (depends on the file system but could be informative to decide whether to download or not)?
- Do we need a service/search interface which resolves all given PIDs/URLs to make the PID record searchable by name, e.g. to enable a researcher to find a person or instrument by name?
