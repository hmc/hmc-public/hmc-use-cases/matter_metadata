# Helmholtz Information Profile for Hub Matter

This approach explores a minimal metadata set to describe datasets which are common for Helmholtz Hub Matter, such as:

 - raw experimental data
 - data publications
 - simulation data
 - software

Hopefully, this will increase the Findability of (meta)data in Helmholtz according to the FAIR principles. The repository comprises
 - a [Metadata Schema for Hub Matter](./matter_schema/metadata_hub_matter.md) (current draft)
 - its [mapping to NXDL](./mapping/nexus_mapping.md), the schema of the NeXus data format.

Please check the following examples

 - [Example 1](./examples/example_1.md)
 - [Example 2](./examples/example_2.md)
 - [Example 3](./examples/example_3.md)
 - [Example 4](./examples/example_4.md)
 - [Example 5](./examples/example_5.md)
 - [Example 6](./examples/example_6.md)
 - [Example 7](./examples/example_7.md)

to see if provided metadata give you an idea what the data set is about.

If you would like to have your metadata example added, feel free to mail your data set or the filled in metadata schema to [gerrit.guenther@helmholtz-berlin.de](mailto:gerrit.guenther@helmholtz-berlin.de). We welcome any comments or suggestions for improvement by email or creating an [issue](https://codebase.helmholtz.cloud/hmc/hmc-public/hmc-use-cases/matter_metadata/-/issues).

The metada schema should harmonize with other approaches which are currently in development, in particular:
- [GSI metadata schema](https://codebase.helmholtz.cloud/a.k.mistry/metadata-np/-/tree/main) (not public yet) which is currently developed for data sets related to Particle Physics
- [Metadaten for HIL and experiments](https://codebase.helmholtz.cloud/meta-laser/metadaten-for-hil-and-experiments) which is/was developed for laser experiments, mainly at HZDR.

If you are aware of related projects, please let us know.
